<?php

function tentukan_nilai($number) {
    $tampung = $number;
    if($tampung > 80 && $tampung < 100) {
        echo "Sangat Baik";
    } else if ($tampung > 70 && $tampung < 80) {
        echo "Baik";
    } else if ($tampung > 50 && $tampung < 70) {
        echo "Cukup";
    } else {
        echo "Kurang";
    }


}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";
echo tentukan_nilai(76); //Baik
echo "<br>";
echo tentukan_nilai(67); //Cukup
echo "<br>";
echo tentukan_nilai(43); //Kurang
?>